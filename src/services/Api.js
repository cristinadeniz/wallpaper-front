const axios = require('axios').default

const API = axios.create({
  baseURL: 'http://localhost:3000/api'
})

function getAllWallpapers () {
  return API
    .get('/wallpapers')
    .then(res => {
      return res.data
    })
}

export default {
  async signup (newUser) {
    const response = await API.post('/auth/signup', {
      ...newUser
    })
    return response.data
  },
  async login (user) {
    const response = await API.post('/auth/login', {
      ...user
    })
    return response.data
  },

  getAllWallpapers
}
